﻿#include <ArduinoJson.h>
#include <TimeLib.h>
#include "SSD1306Spi.h"
#include "SSD1306Wire.h"  // legacy include: `#include "SSD1306.h"`

// Include the UI lib
#include "OLEDDisplayUi.h"

// Include custom images
#include "images.h"

#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

// Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
#define SEVENTY_YEARS 2208988800UL
#define MAKELONG(a, b) \
   ((uint32_t)(((uint16_t)(((uint32_t)(a)) & 0xffff)) | ((uint32_t)((uint16_t)(((uint32_t)(b)) & 0xffff))) << 16))

// A UDP instance to let us send and receive packets over UDP
WiFiUDP udp;

unsigned int localPort = 2390;       // local port to listen for UDP packets
const int NTP_PACKET_SIZE = 48;      // NTP time stamp is in the first 48 bytes of the message
byte packetBuffer[NTP_PACKET_SIZE];  //buffer to hold incoming and outgoing packets

#define SSID "<SSID>"
#define PASSWD_WIFI "<WIFI PASSWORD>"
#define PERIOD_HTTP_REQ 60000   // Perform HTTP request every n milliseconds
#define PERIOD_NTP_REQ 1800000  // Perform time server query every n milliseconds
#define API_KEY "<openweathermap.org API KEY>"
#define LED D4

// Initialize the OLED display using Wire library
SSD1306Wire display(0x3c, D1, D2);

OLEDDisplayUi ui(&display);

int screenW = 128;
int screenH = 64;
int clockCenterX = screenW / 2;
int clockCenterY = ((screenH - 16) / 2) + 16;  // top yellow part is 16 px height
int clockRadius = 23;

struct WeatherInf {
   String City;
   double Temp;
   String Description;
   double Pressure;
   double Humidity;
   const Image *Icon;

   WeatherInf() {
      City = "<CITY>";
      Temp = 21.0;
      Description = "<DESCRIPTION>";
      Pressure = 1000.0;
      Humidity = 50.0;
      Icon = &Images[0];
   }
};

WeatherInf weatherInf;

// utility function for digital clock display: prints leading 0
String addPaddingZero(int value) {
   if (value < 10) {
      String i = '0' + String(value);
      return i;
   }
   else {
      return String(value);
   }
}

/**
 * Shows the current weather information.
 */
void page01(OLEDDisplay *display, OLEDDisplayUiState *state, int16_t x, int16_t y) {
   display->setTextAlignment(TEXT_ALIGN_LEFT);
   display->setFont(ArialMT_Plain_16);

   display->drawString(x, y + 6, weatherInf.City);
   display->drawString(x, y + 22, String(weatherInf.Temp, 1) + " °C");

   // Description can be too long for font size 16
   display->setFont(ArialMT_Plain_10);
   display->drawStringMaxWidth(x, y + 38, 128, weatherInf.Description);
}

/**
 * Draws a digital clock.
 */
void page02(OLEDDisplay *display, OLEDDisplayUiState *state, int16_t x, int16_t y) {
   String timeNow = String(hour()) + ":" + addPaddingZero(minute()) + ":" + addPaddingZero(second());
   display->setTextAlignment(TEXT_ALIGN_CENTER);
   display->setFont(ArialMT_Plain_24);
   display->drawString(clockCenterX + x, y + 12, timeNow);
   display->setFont(ArialMT_Plain_16);
   String dateNow = addPaddingZero(day()) + "." + addPaddingZero(month()) + "." + addPaddingZero(year());
   display->drawString(clockCenterX + x, y + 38, dateNow);
}

/**
 * Draws a weather logo and the temperature.
 */
void page03(OLEDDisplay *display, OLEDDisplayUiState *state, int16_t x, int16_t y) {
   // Draw temperature left aligned
   display->setTextAlignment(TEXT_ALIGN_LEFT);
   display->setFont(ArialMT_Plain_24);
   display->drawString(x, y + 22, String(weatherInf.Temp, 1) + "°C");

   // Draw the icon right aligned. Only show icon when display stands still.
   if (state->frameState == FIXED) {
      const Size &s = weatherInf.Icon->size;
      int16_t h = s.height;
      int16_t w = s.width;
      display->drawXbm(128 - w, y + 10, w, h, weatherInf.Icon->data);
   }
}

// This array keeps function pointers to all frames
// frames are the single views that slide in
FrameCallback frames[] = {page03, page01, page02};

// how many frames are there?
int frameCount = sizeof(frames) / sizeof(frames[0]);

void setup() {
   pinMode(LED, OUTPUT);
   digitalWrite(LED, HIGH);

   Serial.begin(115200);
   Serial.println();

   // The ESP is capable of rendering 60fps in 80Mhz mode
   // but that won't give you much time for anything else
   // run it in 160Mhz mode or just set it to 30 fps
   ui.setTargetFPS(60);

   // Display each frame for approx. 10 seconds
   ui.setTimePerFrame(10000);

   // Customize the active and inactive symbol
   ui.setActiveSymbol(activeSymbol);
   ui.setInactiveSymbol(inactiveSymbol);

   // You can change this to
   // TOP, LEFT, BOTTOM, RIGHT
   ui.setIndicatorPosition(TOP);

   // Defines where the first frame is located in the bar.
   ui.setIndicatorDirection(LEFT_RIGHT);

   // You can change the transition that is used
   // SLIDE_LEFT, SLIDE_RIGHT, SLIDE_UP, SLIDE_DOWN
   ui.setFrameAnimation(SLIDE_LEFT);

   // Add frames
   ui.setFrames(frames, frameCount);

   // Initialising the UI will init the display too.
   ui.init();

   display.flipScreenVertically();

   unsigned long secsSinceStart = millis();

   // subtract seventy years:
   unsigned long epoch = secsSinceStart - SEVENTY_YEARS * SECS_PER_HOUR;
   setTime(epoch);

   // Switch on LED as long as WiFi is not connected
   digitalWrite(LED, LOW);

   for (uint8_t t = 4; t > 0; t--) {
      Serial.printf("[SETUP] WAIT %d...\n", t);
      Serial.flush();
      delay(1000);
   }

   WiFi.mode(WIFI_STA);
   WiFi.begin(SSID, PASSWD_WIFI);

   while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
   }
   Serial.println("");

   digitalWrite(LED, HIGH);  // Switch off LED
   Serial.println("WiFi connected");
   Serial.println("IP address: ");
   Serial.println(WiFi.localIP());

   Serial.println("Starting UDP");
   udp.begin(localPort);
   Serial.print("Local port: ");
   Serial.println(udp.localPort());
}

void performWeatherAPIRequest() {
   // Wait for WiFi connection
   if ((WiFi.status() == WL_CONNECTED)) {
      HTTPClient http;

      Serial.print("[HTTP] begin...\n");

      // Create query string
      // String query = "/data/2.5/weather?q=Bremen,de&units=metric&lang=de&APPID=";
      String query = "/data/2.5/weather?q=Bremen,de&units=metric&APPID=";
      query += API_KEY;

      // configure traged server and url
      http.begin("api.openweathermap.org", 80, query.c_str());  //HTTP

      Serial.print("[HTTP] GET...\n");
      // start connection and send HTTP header
      int httpCode = http.GET();
      if (httpCode) {
         // HTTP header has been send and Server response header has been handled
         Serial.printf("[HTTP] GET... code: %d\n", httpCode);

         if (httpCode == 200) {
            // Successful request
            String payload = http.getString();
            Serial.println(payload);
            // Deserialize json
            DynamicJsonDocument doc(2048);
            auto err = deserializeJson(doc, payload);
            if (err) {
               Serial.println("JSON deserialization failed.");
            }
            else {
               Serial.println("JSON deserialization succeeded.");
               parseAPIResponse(doc);
            }
         }
      }
      else {
         Serial.print("[HTTP] GET... failed, no connection or no HTTP server\n");
      }
   }
}

void parseAPIResponse(const ArduinoJson::DynamicJsonDocument &data) {
   // Store relevant weather information:
   weatherInf.Description = (const char *)data["weather"][0]["description"];
   weatherInf.Icon = FindIcon((const char *)data["weather"][0]["icon"]);
   weatherInf.City = (const char *)data["name"];
   weatherInf.Temp = (double)data["main"]["temp"];
   // Make first letter of weather description uppercase
   weatherInf.Description[0] &= ~('a' - 'A');
}

const Image *FindIcon(const char *szIcon) {
   const Image *pResult = nullptr;

   for (auto &img : Images) {
      if (String(img.name) == szIcon) {
         Serial.print("Found icon \"");
         Serial.print(szIcon);
         Serial.println("\".");
         pResult = &img;
         break;
      }
   }

   if (!pResult) {
      Serial.print("Icon \"");
      Serial.print(szIcon);
      Serial.println("\" not found.");
      return &Images[0];
   }
   return pResult;
}

#define NTP_TIMEOUT 1000

void loop() {
   static int millisReqSent = 0;
   // millis() result when the previous HTTP request was performed
   static int millisPrevHTTPReq = millis() - PERIOD_HTTP_REQ;
   // millis() result when the previous time server query was performed
   static int millisPrevNTPReq = millis() - PERIOD_NTP_REQ;

   int remainingTimeBudget = ui.update();

   // Perform weather API HTTP request every PERIOD_HTTP_REQ milliseconds
   if (!millisPrevHTTPReq || ((millis() - millisPrevHTTPReq) >= PERIOD_HTTP_REQ)) {
      // Switch on LED for the duration of the request
      digitalWrite(LED, LOW);

      millisPrevHTTPReq = millis();
      performWeatherAPIRequest();

      digitalWrite(LED, HIGH);
   }

   // Perform NTP server request
   if ((millis() - millisPrevNTPReq) >= PERIOD_NTP_REQ) {
      if (!millisReqSent) {
         // Switch on LED for the duration of the request
         digitalWrite(LED, LOW);
         sendNTPPacket();
         millisReqSent = millis();
      }
      else if ((millis() - millisReqSent) <= NTP_TIMEOUT) {
         if (getNTPServerResponse()) {
            millisPrevNTPReq = millis();
            millisReqSent = 0;
            digitalWrite(LED, HIGH);
         }
      }
      else {
         Serial.println("NTP server request timed out.");
         millisReqSent = 0;
         millisPrevNTPReq += (NTP_TIMEOUT + 10000);  // Check again in 10 seconds
         digitalWrite(LED, HIGH);
      }
   }
}

// Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
#define SEVENTY_YEARS 2208988800UL

bool getNTPServerResponse() {
   int cb = udp.parsePacket();
   if (!cb) return false;

   Serial.print("UDP packet received, length = ");
   Serial.println(cb);
   // We've received a packet, read the data from it
   udp.read(packetBuffer, NTP_PACKET_SIZE);  // read the packet into the buffer

   //the timestamp starts at byte 40 of the received packet and is four bytes,
   // or two words, long. First, esxtract the two words:

   unsigned long hiWord = word(packetBuffer[40], packetBuffer[41]);
   unsigned long loWord = word(packetBuffer[42], packetBuffer[43]);
   // combine the four bytes (two words) into a long integer
   // this is NTP time (seconds since Jan 1 1900):
   unsigned long secsSince1900 = MAKELONG(loWord, hiWord);
   Serial.print("Seconds since Jan 1 1900 = ");
   Serial.println(secsSince1900);

   // now convert NTP time into everyday time:
   Serial.print("Unix time = ");
   // subtract seventy years:
   unsigned long epoch = secsSince1900 - SEVENTY_YEARS;
   // print Unix time:
   Serial.println(epoch);

   // print the hour, minute and second:
   Serial.print("The UTC time is ");  // UTC is the time at Greenwich Meridian (GMT)
   int h = (epoch % 86400L) / 3600;
   Serial.print(h);  // print the hour (86400 equals secs per day)
   Serial.print(':');
   int m = (epoch % 3600) / 60;
   if (m < 10) {
      // In the first 10 minutes of each hour, we'll want a leading '0'
      Serial.print('0');
   }
   Serial.print(m);  // print the minute (3600 equals secs per minute)
   Serial.print(':');
   int s = epoch % 60;
   if (s < 10) {
      // In the first 10 seconds of each minute, we'll want a leading '0'
      Serial.print('0');
   }
   Serial.println(s);  // print the second

   // Adjust time
   setTime(epoch + SECS_PER_HOUR);

   return true;
}

#define NTP_SERVER_NAME "europe.pool.ntp.org"

// send an NTP request to the time server at the given address
void sendNTPPacket() {
   IPAddress timeServerIP;  // The NTP server address

   // Get a random server from the pool
   WiFi.hostByName(NTP_SERVER_NAME, timeServerIP);

   Serial.println("Sending NTP packet...");
   // set all bytes in the buffer to 0
   memset(packetBuffer, 0, NTP_PACKET_SIZE);
   // Initialize values needed to form NTP request
   // (see URL above for details on the packets)
   packetBuffer[0] = 0b11100011;  // LI, Version, Mode
   packetBuffer[1] = 0;           // Stratum, or type of clock
   packetBuffer[2] = 6;           // Polling Interval
   packetBuffer[3] = 0xEC;        // Peer Clock Precision
   // 8 bytes of zero for Root Delay & Root Dispersion
   packetBuffer[12] = 49;
   packetBuffer[13] = 0x4E;
   packetBuffer[14] = 49;
   packetBuffer[15] = 52;

   // all NTP fields have been given values, now
   // you can send a packet requesting a timestamp:
   udp.beginPacket(timeServerIP, 123);  //NTP requests are to port 123
   udp.write(packetBuffer, NTP_PACKET_SIZE);
   udp.endPacket();
}
